
/**
 * Created by ducanh54 on 09/03/2015.
 * Edit by thachnh_58.
 */
import com.avaje.ebean.Ebean;
import models.Student;
import models.Thesis;
import play.*;
import play.Application;
import play.api.mvc.EssentialFilter;
import play.filters.csrf.CSRFFilter;
import play.libs.Yaml;

import java.util.List;
import java.util.Map;

public class Global extends GlobalSettings {

    @Override
    public void onStart(Application application) {
        Logger.info("Application has started");
        InitialData.insert(application);
    }

    static class InitialData {
        public static void insert(Application app) {
            Map<String, List<Object>> all = (Map<String, List<Object>>)Yaml.load("initial-data.yml");
            if (Ebean.find(Student.class).findRowCount() == 0) {
                Ebean.save(all.get("user_accounts"));
            }
        }
    }

    public void onStop(Application app) {
            Logger.info("Application shutdown...");
        }

    public <T extends EssentialFilter> Class<T>[] filters() {
            return new Class[]{CSRFFilter.class};
    }
}
