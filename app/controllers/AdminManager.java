package controllers;

import com.avaje.ebean.Page;
import models.*;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.message.list_conversation;
import views.html.profile;
import views.html.students.catalog;
import views.html.teacher.teacher;
import play.mvc.Security;
import views.html.thesis.thesis;
import views.html.thesis_register.thesis_manager;
import views.html.document.document;
import views.html.see_profile;

/**
 * Created by ducanh54 on 08/04/2015.
 */
@Security.Authenticated(Secured.class)
public class AdminManager extends Controller {


    public static Result profile() {
        return ok(profile.render());
    }

    public static Result list_student(Integer page){
            Page<Student> students = Student.find(page);
            return ok(catalog.render(students));
    }

    public static Result list_teacher(Integer page){
        Page<Teacher> teacherPage = Teacher.find(page);
        return ok(teacher.render(teacherPage));
    }

    public static Result listThesis(Integer page) {
        Page<Thesis> thesises = Thesis.find(page);
        return ok(thesis.render(thesises));
    }

    public static Result listStudentRegister(Integer page) {
        Page<Student> studentPage = Student.findStudent(page);
        return ok(thesis_manager.render(studentPage));
    }

    public static Result listDocument(Integer page) {
            Page<Document> documentPage = Document.find(page);
            return ok(document.render(documentPage));
    }

    public static Result seeProfile(String email) {
        Student student = Student.findByEmail(email);
        return ok(see_profile.render(student));
    }

    public static Result conversation(Integer page) {
        Page<Message> messagePage = Message.find(page);
        return ok(list_conversation.render(messagePage));
    }

}
