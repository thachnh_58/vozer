package controllers;

import models.Student;
import models.Teacher;
import models.Thesis;
import play.data.Form;
import play.mvc.*;
import views.html.*;

import static play.data.Form.form;

public class Application extends Controller {

    public static class Login {
        public String email;
        public String password;
    }

    public static Result index() {
        if(session().get("email") == null) {
            return redirect(routes.Application.login());
        } else {
            if(session().get("type").equals("0")) {
                return redirect(routes.AdminManager.profile());
            } else {
                return redirect(routes.AdminManager.profile());
            }
        }
    }

    public static Result login() {
        return ok(login.render(form(Login.class)));
    }

    public static Result authenticate() {
        Form<Login> loginForm = form(Login.class).bindFromRequest();
        String email = loginForm.get().email;
        String password = loginForm.get().password;
        session().clear();

        if (Student.authenticate(email, password,"0") != null){
            session("type", "0");
            session("email", "");
            session("name", Student.findByEmail(email).getName().toString());
            session("countStudent", String.valueOf(Student.find.where().eq("type", "1").findRowCount()));
            session("countThesis", String.valueOf(Thesis.find.where().findRowCount()));
            session("countTeacher", String.valueOf(Teacher.find.where().eq("type", "2").findRowCount()));
            return redirect(routes.AdminManager.list_student(0));
        }

        if (Student.authenticate(email, password, "1") != null) {
            session("type", "1");
            session("email", email);
            session("name", Student.findByEmail(email).getName().toString());
            session("countStudent", String.valueOf(Student.find.where().eq("type", "1").findRowCount()));
            session("countThesis", String.valueOf(Thesis.find.where().findRowCount()));
            session("countTeacher", String.valueOf(Teacher.find.where().eq("type", "2").findRowCount()));
            session("thesis_status", Student.findByEmail(email).thesis_status);
            if ( Student.findByEmail(email).thesis_register == null) {
                session("thesis_register", "none");
            } else {
                session("thesis_register", Student.findByEmail(email).thesis_register);
            }
            return redirect(routes.AdminManager.profile());
        }

        if (Teacher.authenticate(email, password, "2") != null) {
            session("type", "2");
            session("email", email);
            session("name", Teacher.findByEmail(email).getName().toString());
            session("countStudent", String.valueOf(Student.find.where().eq("type", "1").findRowCount()));
            session("countThesis", String.valueOf(Thesis.find.where().findRowCount()));
            session("countTeacher", String.valueOf(Teacher.find.where().eq("type", "2").findRowCount()));
            return redirect(routes.AdminManager.profile());
        }

        flash("error", "Invalid email/or password");
        return redirect(routes.Application.login());

    }

    public static Result logout() {
        session().clear();
        return redirect(routes.Application.index());
    }
}
