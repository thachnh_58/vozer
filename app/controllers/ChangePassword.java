package controllers;

import models.Student;
import models.Teacher;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;

import views.html.change_password;


/**
 * Created by ducanh54 on 09/04/2015.
 */
public class ChangePassword extends Controller {
    public String passwordCurrent;
    public String newPassword;
    public String repeatNewPassword;

    public static Result changePassword() {
        return ok(change_password.render(Form.form(ChangePassword.class)));
    }

    public static Result save() {
        Form<ChangePassword> changePasswordForm = Form.form(ChangePassword.class).bindFromRequest();
        String _passwordCurrent = changePasswordForm.get().passwordCurrent;
        String _newPassword = changePasswordForm.get().newPassword;
        String _repeatNewPassword = changePasswordForm.get().repeatNewPassword;
        if (Student.find.where().eq("email", session().get("email").toString()).eq("password", _passwordCurrent).findUnique() == null) {
            flash("error", "Mật khẩu hiện tại không đúng");
            return redirect(routes.ChangePassword.changePassword());
        }
        if (!_newPassword.equals(_repeatNewPassword)) {
            flash("error", "Mật khẩu mới không trùng nhau");
            return redirect(routes.ChangePassword.changePassword());
        }
        if (_newPassword.equals("") || _passwordCurrent.equals("") || _repeatNewPassword.equals("")) {
            flash("error", "Form không được để trống");
            return redirect(routes.ChangePassword.changePassword());
        }

        if (session().get("type").equals("1")) {
            Student student = Student.findByEmail(session().get("email"));
            student.password = _newPassword;
            student.update();
            flash("success", "Thay đổi mật khẩu thành công");
            return redirect(routes.AdminManager.profile());
        }

        if (session().get("type").equals("2")) {
            Teacher teacher = Teacher.findByEmail(session().get("email"));
            teacher.password = _newPassword;
            teacher.update();
            flash("success", "Thay đổi mật khẩu thành công");
            return redirect(routes.AdminManager.list_teacher(0));
        }
        flash("error", "Đổi password không thành công");
        return redirect(routes.ChangePassword.changePassword());
    }
}
