package controllers;

import models.Student;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;

import views.html.form_forgot_password;

/**
 * Created by ducanh54 on 23/04/2015.
 */
public class ForgotPassword extends Controller {
    public String currentEmail;


    public ForgotPassword() {}
    public static Result forgotPassword() {
        return ok(form_forgot_password.render(Form.form(ForgotPassword.class)));
    }

    public static Result sendPassword() {
        Form<ForgotPassword> boundForm = Form.form(ForgotPassword.class).bindFromRequest();
        if(Student.find.where().eq("email", boundForm.get().currentEmail).findUnique() != null) {
            flash("success", String.format("Password của bạn sẽ được gửi về email %s, kiểm tra email để lấy lại mật khẩu! ", boundForm.get().currentEmail));
            String password = Student.find.where().eq("email", boundForm.get().currentEmail).findUnique().password;
            Registers.send(boundForm.get().currentEmail, "Ban vua su dung chuc nang tim mat khau: \n " + boundForm.get().currentEmail, password);
            return redirect(routes.Application.login());
        }
        flash("success", String.format("Email %s không tồn tại", boundForm.get().currentEmail));
        return badRequest(form_forgot_password.render(boundForm));
    }

}
