package controllers;

import com.avaje.ebean.Ebean;
import models.RandomStringGenerator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.SimpleEmail;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;

import views.html.edit;
import views.html.students.register;

import models.Student;

import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ducanh54 on 09/03/2015.
 * Edit by Nguyen_Thach on 10/03/2015.
 */

@Security.Authenticated(Secured.class)
public class Registers extends Controller {

    private static final Form<Student> studentFormRegister = Form.form(Student.class);
    private static final Form<Student> editprofile = Form.form(Student.class);

    public static Result register() {
        return ok(register.render(studentFormRegister));
    }


    public static Result details(Student student){
        if (student == null){
            return notFound(String.format("Student % does not exits", student.email));
        }
        Form<Student> filledForm = editprofile.fill(student);
        return ok(edit.render(filledForm));
    }

    public static Result edit(String email){
        session("email", email);
        Student student = Student.findByEmail(email);
        Form<Student> filledForm = editprofile.fill(student);
        return ok(edit.render(filledForm));
    }

    public static Result save() {
        Form<Student> boundForm = studentFormRegister.bindFromRequest();
        if (boundForm.hasErrors()){
            flash("error", "Please correct the form below.");
            return badRequest(register.render(boundForm));
        }
        Student student = boundForm.get();
        String newPassword = "";
        if (!isEmailValid(boundForm.get().email)) {
            flash("error", "Email is not valid");
            return badRequest(register.render(boundForm));
        } else {
            if(student.find.where().like("email", boundForm.get().email).findRowCount() > 0) {
                flash("error", "This email address already in use");
                return badRequest(register.render(boundForm));
            }
            if (student.id == null) {
                    try {
                        newPassword = RandomStringGenerator.generateRandomString(10, RandomStringGenerator.Mode.ALPHANUMERIC);
                        student.password = newPassword;
                        student.date = Calendar.getInstance().getTime();
                        student.type = String.valueOf(1);
                        student.thesis_status = "none";
                        student.thesis_register = "none";
                        student.time_start = 0L;
                        student.time_end = 0L;
                        student.birthday = "";
                        student.address = "";
                        student.numberphone = "";
                        student.whereborn = "";
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    student.save();
            } else {
                Ebean.update(student);
            }
        }
        send(boundForm.get().email, boundForm.get().email, newPassword);
        session("countStudent", String.valueOf(Student.find.where().eq("type", "1").findRowCount()));
        flash("success", String.format("Successfully added Student %s", student));
        return redirect(routes.AdminManager.list_student(0));
    }

    public static Result changeProfile() {
        Form<Student> boundForm = editprofile.bindFromRequest();
        Student formstudent = boundForm.get();
        Student student = Student.findByEmail(formstudent.email);
        student.name = formstudent.name;
        student.specialized = formstudent.specialized;
        student.course = formstudent.course;
        student.birthday = formstudent.birthday;
        student.address = formstudent.address;
        student.numberphone = formstudent.numberphone;
        student.whereborn = formstudent.whereborn;
        student.update();
        return redirect(routes.AdminManager.profile());
    }

    public static Result delete(Student student){
        if (student == null) {
            return notFound(String.format("Student %s does not exits", student.email));
        }
        student.delete();
        return redirect(routes.AdminManager.list_student(0));
    }


    public static Result send(String addressEmail, String username, String password) {
        Email email = new SimpleEmail();
        try {
            email.setHostName("smtp.gmail.com");
            email.setSmtpPort(465);
            email.setAuthentication("se2015.uet@gmail.com", "asdassd1");
            email.setSSLOnConnect(true);
            email.setFrom(addressEmail);
            email.addTo(addressEmail);
            email.setSubject("Thông tin tài khoản");
            email.setMsg("Chào mừng bạn đến với hệ thống đăng ký luận văn Trường Đại học Công nghệ.\n" + "Thông tin đăng nhập của bạn là:\n" + "Username: " + username + "\n" + "Password is: " + password);
            email.send();
        } catch (Exception e) {
            e.printStackTrace();
        }

       return ok();
    }

    public static boolean isEmailValid(String email) {
        String regExpn = "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if (matcher.matches()) {
            return true;
        }
        else {
            return false;
        }
    }

}
