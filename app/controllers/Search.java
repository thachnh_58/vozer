package controllers;

import com.avaje.ebean.Page;
import models.Student;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.students.catalog;

/**
 * Created by ducanh54 on 01/05/2015.
 */
public class Search extends Controller {
    public String keysearch;
    public String select;

    public Search() {}

    public static Result searchStudent(Integer page) {
        Form<Search> studentForm = Form.form(Search.class).bindFromRequest();
        if(studentForm.get().select.equals("Mã học viên")) {
            Page<Student> students = Student.searchStcode(page, studentForm.get().keysearch);
            return ok(catalog.render(students));
        }
        if(studentForm.get().select.equals("Theo tên")) {
            Page<Student> students = Student.searchName(page, studentForm.get().keysearch);
            return ok(catalog.render(students));
        }
        if(studentForm.get().select.equals("Theo khoa")) {
            Page<Student> students = Student.searchSpecialized(page, studentForm.get().keysearch);
            return ok(catalog.render(students));
        }
        Page<Student> students = Student.searchStcode(page, studentForm.get().keysearch);
        return ok(catalog.render(students));
    }

    public static Result searchTeacher(Integer page) {
        return TODO;
    }

    public static Result searchThesis(Integer page) {
        return TODO;
    }

    public static Result searchDocuemnt(Integer page) {
        return TODO;
    }

}
