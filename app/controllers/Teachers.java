package controllers;

import models.Document;
import models.RandomStringGenerator;
import org.apache.commons.io.FileUtils;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.SimpleEmail;
import play.mvc.Http.MultipartFormData;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;

import views.html.teacher.teacher_form;
import views.html.document.upload_document;

import models.Teacher;

import java.io.*;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Nguyen_Thach on 04/22/2015.
 */

@Security.Authenticated(Secured.class)
public class Teachers extends Controller {

    private static final Form<Teacher> teacherform = Form.form(Teacher.class);

    private static final Form<Document> documentform = Form.form(Document.class);

    public static Result register() {
        return ok(teacher_form.render(teacherform));
    }

    public static Result uploadDocument() {
        return ok(upload_document.render(documentform));
    }

    public static Result saveDocument() {
        Form<Document> boundForm = documentform.bindFromRequest();
        Document document = boundForm.get();
        MultipartFormData body = request().body().asMultipartFormData();
        MultipartFormData.FilePart pdf = body.getFile("pdf");

        if (pdf != null) {
            String fileName = pdf.getFilename();
            String contentType = pdf.getContentType();
            File file = pdf.getFile();
            // File file = request().body().asRaw().asFile();
            try {
                FileUtils.moveFile(file, new File("public/images/products", fileName));
            } catch (IOException e) {
                e.printStackTrace();
            }

            document.linkURL = (new File("public/images/products", fileName)).getPath();
            document.save();
            /*
            file.renameTo(new File("/se2015/vozer/target/upload/documents/" + fileName));
            Teacher teacher = Teacher.findByTeacherCode("a");
            teacher.email = "/se2015/" + fileName;
            teacher.update();
            */
            return redirect(routes.AdminManager.listDocument(0));

        } else {
            flash("error", "Missing file");
            return redirect(routes.Application.index());
        }
    }

    public static Result download(String getPath) {
        File file = new File(getPath);
        return ok(file);
    }

    public static Result getDocument() {
        return TODO;
    }



    public static Result details(Teacher teacher){
        if (teacher == null){
            return notFound(String.format("Teacher % does not exits", teacher.email));
        }
        Form<Teacher> filledForm = teacherform.fill(teacher);
        return ok(teacher_form.render(filledForm));
    }

    public static Result save() {
        Form<Teacher> boundForm = teacherform.bindFromRequest();
        if (boundForm.hasErrors()){
            flash("error", "Please correct the form below.");
            return badRequest(teacher_form.render(boundForm));
        }
        Teacher teacher = boundForm.get();
        String newPassword = "";
        if (!isEmailValid(boundForm.get().email)) {
            flash("error", "Email is not valid");
            return badRequest(teacher_form.render(boundForm));
        } else {
            if(Teacher.find.where().like("email", boundForm.get().email).findRowCount() > 0) {
                flash("error", "This email address already in use");
                return badRequest(teacher_form.render(boundForm));
            }
            if (teacher.id == null) {
                try {
                    newPassword = RandomStringGenerator.generateRandomString(10, RandomStringGenerator.Mode.ALPHANUMERIC);
                    teacher.password = newPassword;
                    teacher.date = Calendar.getInstance().getTime();
                    teacher.type = String.valueOf(2);
                } catch (Exception e) {
                }
                teacher.save();
            } else {
                teacher.update();
            }
        }
        send(boundForm.get().email, boundForm.get().email, newPassword);
        flash("success", String.format("Successfully added Teacher %s", teacher));
        return redirect(routes.AdminManager.list_teacher(0));
    }

    public static Result delete(Teacher teacher){
        if (teacher == null) {
            return notFound(String.format("Teacher %s does not exits", teacher.email));
        }
        teacher.delete();
        return redirect(routes.AdminManager.list_teacher(0));
    }

    public static Result send(String addressEmail, String username, String password) {
        Email email = new SimpleEmail();
        try {
            email.setHostName("smtp.gmail.com");
            email.setSmtpPort(465);
            email.setAuthentication("se2015.uet@gmail.com", "asdassd1");
            email.setSSLOnConnect(true);
            email.setFrom(addressEmail);
            email.addTo(addressEmail);
            email.setSubject("SE 2015");
            email.setMsg("Username: " + username + "\n" + "Password is: " + password);
            email.send();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return ok();
    }

    public static boolean isEmailValid(String email) {
        String regExpn = "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if (matcher.matches()) {
            return true;
        }
        else {
            return false;
        }
    }
}
