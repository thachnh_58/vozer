package controllers;

import models.Student;
import models.Thesis;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.SimpleEmail;
import play.mvc.Security;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;

import views.html.thesis.form_thesis;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by ducanh54 on 10/04/2015.
 */

@Security.Authenticated(Secured.class)
public class Thesises extends Controller {
    private static final Form<Thesis> thesisForm = Form.form(Thesis.class);

    public static Result register() {
        return ok(form_thesis.render(thesisForm));
    }

    public static Result details(Thesis thesis){
        if (thesis == null){
            return notFound(String.format("Luận văn % không tồn tại", thesis.thread));
        }
        Form<Thesis> filledForm = thesisForm.fill(thesis);
        return ok(form_thesis.render(filledForm));
    }

    public static Result save() {
        Form<Thesis> boundForm = thesisForm.bindFromRequest();
        if (boundForm.hasErrors()){
            flash("error", "Biểu mẫu nhập không đúng !");
            return badRequest(form_thesis.render(boundForm));
        }

        Thesis thesis = boundForm.get();
        if(thesis.find.where().like("thesis_code", boundForm.get().thesis_code).findUnique() != null) {
            flash("error", "Mã luận văn đã tồn tại !");
            return badRequest(form_thesis.render(boundForm));
        } else {
            if (thesis.id == null) {
                thesis.update_time = Calendar.getInstance().getTime();
                thesis.update_email = Student.findByEmail(session().get("email")).getName();
                thesis.save();
            } else {
                thesis.update_time = Calendar.getInstance().getTime();
                thesis.update_email = Student.findByEmail(session().get("email")).getName();
                thesis.update();
            }
        }
        flash("success", "Thêm luận văn thành công !");
        session("countThesis", String.valueOf(Thesis.find.where().findRowCount()));
        return redirect(routes.AdminManager.listThesis(0));
    }

    public static Result student_register(String thesis_code) {
        if(Student.find.where().eq("email", session().get("email")).in("thesis_status", new String[]{"wait", "progress", "done"}).findUnique() != null) {
            flash("error", "Bạn chỉ được phép đăng ký 1 luận văn");
        } else {
            Student student = Student.findByEmail(session().get("email"));
            student.thesis_register = thesis_code;
            student.thesis_status = "wait";
            student.update();
            flash("success", "Đăng ký thành công, hãy chờ giáo vụ xét duyệt");
            session("thesis_status", Student.findByEmail(session().get("email")).thesis_status);
            session("thesis_register", Student.findByEmail(session().get("email")).thesis_register);
        }

        return redirect(routes.AdminManager.listThesis(0));
    }

    public static Result student_progress(String emailAddress) {
        Student student = Student.findByEmail(emailAddress);
        student.thesis_status = "progress";
        student.time_start = Calendar.getInstance().getTimeInMillis();
        student.update();
        session("thesis_status", "progress");
        Email email = new SimpleEmail();
        try {
            email.setHostName("smtp.gmail.com");
            email.setSmtpPort(465);
            email.setAuthentication("se2015.uet@gmail.com", "asdassd1");
            email.setSSLOnConnect(true);
            email.setFrom(emailAddress);
            email.addTo(emailAddress);
            email.setSubject("SE 2015");
            String mBody = "Giao vu da chap nhan dang ky luan van cua ban vao luc: " + convertDate(Student.findByEmail(emailAddress).time_start);
            email.setMsg(mBody);
            email.send();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return redirect(routes.AdminManager.listStudentRegister(0));

    }

    public static Result student_done() {
        Student student = Student.findByEmail(session().get("email"));
        student.thesis_status = "done";
        student.time_end = Calendar.getInstance().getTimeInMillis();
        student.update();
        session("thesis_status", Student.findByEmail(session().get("email")).thesis_status);
        flash("success", "Bạn đã hoàn thành luận văn của mình, hãy chờ phản hồi từ giáo vụ và giáo viên hướng dẫn");
        return redirect(routes.AdminManager.profile());
    }

    public static Result student_delete(String emailAddress) {
        Student student = Student.findByEmail(emailAddress);
        student.thesis_status = "none";
        student.thesis_register = "none";
        student.time_end = 0L;
        student.time_start = 0L;
        student.update();
        session("thesis_status", "none");
        Email email = new SimpleEmail();
        try {
            email.setHostName("smtp.gmail.com");
            email.setSmtpPort(465);
            email.setAuthentication("se2015.uet@gmail.com", "asdassd1");
            email.setSSLOnConnect(true);
            email.setFrom(emailAddress);
            email.addTo(emailAddress);
            email.setSubject("SE 2015");
            String mBody = "Luan van nay khong phu hop voi ban, hay chon mot luan van khac phu hop hon\nThoi gian huy: " + convertDate(Calendar.getInstance().getTimeInMillis());
            email.setMsg(mBody);
            email.send();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return redirect(routes.AdminManager.listStudentRegister(0));
    }

    public static String convertDate(Long time) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        SimpleDateFormat formater = new SimpleDateFormat(
                "dd/MM/yyyy - HH:mm:ss");
        return formater.format(calendar.getTime());
    }
}
