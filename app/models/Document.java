package models;

import com.avaje.ebean.Page;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;

/**
 * Created by ducanh54 on 28/04/2015.
 */
@Entity
public class Document extends Model {
    @Id
    public Long id;

    public String name;

    public String linkURL;

    public String kind;

    public String language;

    public Document() {}

    public Document(String name, String linkURL, String kind, String language) {
        this.name = name;
        this.linkURL = linkURL;
        this.kind = kind;
        this.language = language;
    }

    public static Finder<Long, Document> find = new Finder<Long, Document>(Long.class, Document.class);

    public static Page<Document> find(int page){
        return find.where()
                .orderBy("id asc")
                .findPagingList(10)
                .setFetchAhead(false)
                .getPage(page);
    }
}
