package models;

import com.avaje.ebean.Page;
import play.db.ebean.Model;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by ducanh54 on 07/05/2015.
 */
@Entity
public class Message extends Model {
    @Id
    public Long id;

    public String username;

    public String address;

    public String body;

    public String type;

    public String seen;

    public Long time_current;

    public Message() {}

    public Message(String username, String address, String body, String type, String seen, Long time_current) {
        this.username = username;
        this.address = address;
        this.body = body;
        this.seen = seen;
        this.type = type;
        this.time_current = time_current;
    }

    public static Finder<Long, Message> find = new Finder<Long, Message>(Long.class, Message.class);

    public static Page<Message> find(int page) {
        return find
                .where()
                .findPagingList(10)
                .setFetchAhead(false)
                .getPage(page);
    }
}
