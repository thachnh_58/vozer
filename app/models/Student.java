package models;

import com.avaje.ebean.Page;
import play.data.Form;
import play.data.validation.Constraints;
import play.db.ebean.Model;
import play.mvc.PathBindable;

import javax.persistence.Entity;
import javax.persistence.Id;

import java.util.Date;

import java.util.List;

/**
 * Created by ducanh54 on 09/03/2015.
 */

@Entity
public class Student extends Model implements PathBindable<Student>{

    @Id
    public Long id;
    @Constraints.Required
    public String stcode;
    @Constraints.Required
    public String name;
    @Constraints.Required
    public String specialized;
    @Constraints.Required
    public String course;
    @Constraints.Required
    public String email;

    public String password;

    public Date date;

    public String type;

    public String thesis_register;

    public String thesis_status;

    public Long time_start;

    public Long time_end;

    public String birthday;

    public String whereborn;

    public String address;

    public String numberphone;

    public Student() {}

    public Student(String stcode, String name, String specialized, String course, String email, String password, String type,Date date, String thesis_register, String thesis_status, Long time_start, Long time_end) {
        this.stcode = stcode;
        this.name = name;
        this.course = course;
        this.specialized = specialized;
        this.email = email;
        this.password = password;
        this.type = type;
        this.date = date;
        this.thesis_register = thesis_register;
        this.thesis_status = thesis_status;
        this.time_start = time_start;
        this.time_end = time_end;
    }

    public String toString(){
        return String.format("%s - %s - %s - %s - %s", stcode, name, course, specialized, email);
    }

    public static Student findByEmail(String email){
        return find.where().eq("email", email).findUnique();
    }

    public static List<Student> findById(String term){
        return find.where().eq("id", term).findList();
    }

    public static Page<Student> find(int page){
            return find.where()
                    .like("type", "1")
                    .orderBy("id asc")
                    .findPagingList(10)
                    .setFetchAhead(false)
                    .getPage(page);
    }


    public static Page<Student> findStudent(int page) {
        return find.where()
                .like("type", "1")
                .in("thesis_status", new String[]{"wait","progress","done"})
                .orderBy("id asc")
                .findPagingList(10)
                .setFetchAhead(false)
                .getPage(page);

    }

    public static Page<Student> searchStcode(int page, String keysearch) {
        return find.where().like("stcode", keysearch).orderBy("id asc").findPagingList(10).setFetchAhead(false).getPage(page);
    }

    public static Page<Student> searchName(int page, String keysearch) {
        return find.where().like("name", keysearch).orderBy("id asc").findPagingList(10).setFetchAhead(false).getPage(page);
    }

    public static Page<Student> searchSpecialized(int page, String keysearch) {
        return find.where().like("specialized", keysearch).orderBy("id asc").findPagingList(10).setFetchAhead(false).getPage(page);
    }

    public static Finder<Long, Student> find = new Finder<Long, Student>(Long.class, Student.class);
    @Override
    public Student bind(String key, String value) {
        return findByEmail(value);
    }

    @Override
    public String unbind(String key) {
        return this.email;
    }

    @Override
    public String javascriptUnbind(){
        return this.email;
    }


    public static Student authenticate(String email, String password, String type){
        return find.where().eq("email", email).eq("password", password).eq("type", type).findUnique();
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }
}