package models;

/**
 * Created by ducanh54 on 12/04/2015.
 */

import com.avaje.ebean.Page;
import play.db.ebean.Model;
import play.mvc.PathBindable;

import javax.persistence.Entity;
import javax.persistence.Id;

import java.util.Date;

import java.util.List;

@Entity
public class Teacher extends Model implements PathBindable<Teacher> {

    @Id
    public Long id;

    public String teacher_code;

    public String name;

    public String bithday;

    public String specialized;

    public String email;

    public String password;

    public String type;

    public Date date;

    public Teacher() {}

    public List<Teacher> teachers;

    public Teacher(String teacher_code, String name, String bithday, String specialized, String email, String password, String type) {
        this.teacher_code = teacher_code;
        this.name = name;
        this.bithday = bithday;
        this.specialized = specialized;
        this.email = email;
        this.password = password;
        this.type = type;

    }

    public static Teacher findByEmail(String email){
        return find.where().eq("email", email).findUnique();
    }

    public String toString(){
        return String.format("%s - %s - %s - %s - %s", teacher_code, name, specialized, email);
    }

    public static Finder<Long, Teacher> find = new Finder<Long, Teacher>(Long.class, Teacher.class);

    public static List<Teacher> findById(String term){
        return find.where().eq("id", term).findList();
    }

    public static Page<Teacher> find(int page){
        return find.where()
                .like("type","2")
                .orderBy("id asc")
                .findPagingList(10)
                .setFetchAhead(false)
                .getPage(page);
    }

    public static Teacher findByTeacherCode(String teacher_code) {
        return find.where().eq("teacher_code", teacher_code).findUnique();
    }

    @Override
    public Teacher bind(String key, String value) {
        return findByEmail(value);
    }
    @Override
    public String unbind(String key) {
        return this.email;
    }

    @Override
    public String javascriptUnbind(){
        return this.email;
    }


    public static Teacher authenticate(String email, String password, String type){
        return find.where().eq("email", email).eq("password", password).eq("type", type).findUnique();
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }
}
