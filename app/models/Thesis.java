package models;

import com.avaje.ebean.Page;
import play.data.validation.Constraints;
import play.mvc.PathBindable;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

/**
 * Created by ducanh54 on 08/04/2015.
 */
@Entity
public class Thesis extends Model implements PathBindable<Thesis> {
    @Id
    public Long id;

    @Constraints.Required
    public String thesis_code;

    @Constraints.Required
    public String thread;

    @Constraints.Required
    public String description;

    @Constraints.Required
    public String teacher;

    public Date update_time;

    public String update_email;

    public Thesis(String thesis_code, String thread, String description, String teacher, Date update_time, String update_email) {
        this.thesis_code = thesis_code;
        this.thread = thread;
        this.description = description;
        this.update_time = update_time;
        this.update_email = update_email;
        this.teacher = teacher;
    }

    public static Finder<Long, Thesis> find = new Finder<Long, Thesis>(Long.class, Thesis.class);

    public static Page<Thesis> find(int page){
        return find.where()
                .orderBy("id asc")
                .findPagingList(10)
                .setFetchAhead(false)
                .getPage(page);
    }

    public static Thesis findByThesisCode(String thesis_code) {
        return find.where().eq("thesis_code", thesis_code).findUnique();
    }

    public Thesis bind(String key, String value) {
        return findByThesisCode(value);
    }

    @Override
    public String unbind(String key) {
        return this.thesis_code;
    }

    @Override
    public String javascriptUnbind(){
        return this.thesis_code;
    }
}
