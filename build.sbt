name := """VOZER"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.1"

libraryDependencies ++= Seq(
  javaEbean,
  "org.postgresql" % "postgresql" % "9.3-1100-jdbc41",
  "com.typesafe.play" %% "play-mailer" % "2.4.0-RC1",
  "org.apache.directory.studio" % "org.apache.commons.io" % "2.4",
"com.amazonaws" % "aws-java-sdk" % "1.3.11"
)

libraryDependencies += filters