# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table document (
  id                        bigint not null,
  name                      varchar(255),
  link_url                  varchar(255),
  kind                      varchar(255),
  language                  varchar(255),
  constraint pk_document primary key (id))
;

create table message (
  id                        bigint not null,
  username                  varchar(255),
  address                   varchar(255),
  body                      varchar(255),
  type                      varchar(255),
  seen                      varchar(255),
  time_current              bigint,
  constraint pk_message primary key (id))
;

create table student (
  id                        bigint not null,
  stcode                    varchar(255),
  name                      varchar(255),
  specialized               varchar(255),
  course                    varchar(255),
  email                     varchar(255),
  password                  varchar(255),
  date                      timestamp,
  type                      varchar(255),
  thesis_register           varchar(255),
  thesis_status             varchar(255),
  time_start                bigint,
  time_end                  bigint,
  birthday                  varchar(255),
  whereborn                 varchar(255),
  address                   varchar(255),
  numberphone               varchar(255),
  constraint pk_student primary key (id))
;

create table teacher (
  id                        bigint not null,
  teacher_code              varchar(255),
  name                      varchar(255),
  bithday                   varchar(255),
  specialized               varchar(255),
  email                     varchar(255),
  password                  varchar(255),
  type                      varchar(255),
  date                      timestamp,
  constraint pk_teacher primary key (id))
;

create table thesis (
  id                        bigint not null,
  thesis_code               varchar(255),
  thread                    varchar(255),
  description               varchar(255),
  teacher                   varchar(255),
  update_time               timestamp,
  update_email              varchar(255),
  constraint pk_thesis primary key (id))
;

create sequence document_seq;

create sequence message_seq;

create sequence student_seq;

create sequence teacher_seq;

create sequence thesis_seq;




# --- !Downs

drop table if exists document cascade;

drop table if exists message cascade;

drop table if exists student cascade;

drop table if exists teacher cascade;

drop table if exists thesis cascade;

drop sequence if exists document_seq;

drop sequence if exists message_seq;

drop sequence if exists student_seq;

drop sequence if exists teacher_seq;

drop sequence if exists thesis_seq;

